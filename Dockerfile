FROM httpd:2.4

MAINTAINER Joel M

COPY index.html /usr/local/apache2/htdocs/

CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]

